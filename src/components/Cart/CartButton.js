import classes from './CartButton.module.css';
import { useSelector, useDispatch } from 'react-redux';
import { uiActions } from '../../store/ui-slice';


const CartButton = (props) => {
  const showCart = useSelector((state) => state.ui.cartIsVisible);
  const totalCartItems = useSelector(state => state.cart.totalQuantity);
  console.log(totalCartItems);
  //console.log(showCart)

  const dispatch = useDispatch();

  const toggleCartHandler = () => {
    dispatch(uiActions.toggle());
  }

  return (
    <button className={classes.button} onClick={toggleCartHandler }>
      <span>My Cart</span>
      <span className={classes.badge}>{totalCartItems}</span>
    </button>
  );
};

export default CartButton;

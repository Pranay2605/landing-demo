import { useEffect } from "react";

import Card from "../UI/Card";
import { useSelector, useDispatch } from "react-redux";
import classes from "./Cart.module.css";
import CartItem from "./CartItem";
import { getUsers } from "../../store/user-slice";

const Cart = (props) => {
  const dispatch = useDispatch();
  const {status,users} = useSelector((state) => state.users);
  

  const cartItems = useSelector((state) => state.cart.items);
  
  useEffect(() => {
    dispatch(getUsers(2));
  }, []);

  useEffect(() => {
    if (status === "success") {
      // do anything
      console.log(users);
    }
  }, [status]);
 
  return (
    <Card className={classes.cart}>
      <h2>Your Shopping Cart</h2>
      <ul>
        {cartItems.map((item) => (
          <CartItem
            key={item.id}
            item={{
              id:item.id,
              title: item.name,
              quantity: item.quantity,
              total: item.totalPrice,
              price: item.price,
            }}
          />
        ))}

        {/* {users && users.map((user) => <CartItem item={{title: user.name}} />)} */}
      </ul>
     
    </Card>
  );
};

export default Cart;

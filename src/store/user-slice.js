import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const getUsers = createAsyncThunk(
  "users/getUsers",
    async (temp) => {
      console.log(temp,"temp");
    const res = await fetch(`https://jsonplaceholder.typicode.com/users/${temp}`);
    const data = res.json();
    return data
  }
);


const usersSlice = createSlice({
  name: "users",
  initialState: {
    users: [],
    status: null,
  },
  extraReducers: {
    [getUsers.pending]: (state, action) => {
      state.status = "loading";
    },
    [getUsers.fulfilled]: (state, action) => {
      state.status = "success";
      state.users = action.payload;
    },
    [getUsers.rejected]: (state, action) => {
      state.status = "failed";
    },
    },
  


});

export default usersSlice;

//     extraReducers: (builder) => {
//         builder.addCase(getUsers.fulfilled, (state, action) => {
//             console.log(action.payload);
//         });
//   }
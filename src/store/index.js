import { configureStore } from "@reduxjs/toolkit";
import uiSlice from './ui-slice';
import cartSlice from "./cart-slice";
import usersSlice from "./user-slice";
    
const store = configureStore({
    reducer: {
        ui: uiSlice.reducer,
        cart: cartSlice.reducer,
        users: usersSlice.reducer
    }
});

export default store;